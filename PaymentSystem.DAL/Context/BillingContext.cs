﻿using System.Data.Entity;
using PaymentsSystem.Core;
using PaymentSystem.DAL.Configuration;

namespace PaymentSystem.DAL.Context
{
    class BillingContext : DbContext
    {
        public BillingContext() : base("BillingContextDb")
        {
            
        }

        public IDbSet<Customer> Customers { get; set; }
        public IDbSet<Invoice> Invoices { get; set; }
        public IDbSet<InvoiceItem> InvoiceItems { get; set; }
        public IDbSet<Merchant> Merchants { get; set; }
        public IDbSet<Payment> Payments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new InvoiceEntityConfiguration());
            modelBuilder.Configurations.Add(new CustomerEntityConfiguration());
        }
    }
}