﻿using System.Data.Entity.ModelConfiguration;
using PaymentsSystem.Core;

namespace PaymentSystem.DAL.Configuration
{
    class InvoiceEntityConfiguration : EntityTypeConfiguration<Invoice>
    {
        public InvoiceEntityConfiguration()
        {
            ToTable("Invoices");
            Property(i => i.InvoiceNumber).IsRequired();

            Property(i => i.Notes).HasColumnType("nvarchar(1000)");
        }
    }
}