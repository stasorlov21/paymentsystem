﻿using System.Data.Entity.ModelConfiguration;
using PaymentsSystem.Core;

namespace PaymentSystem.DAL.Configuration
{
    class CustomerEntityConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerEntityConfiguration()
        {
            ToTable("Customers");
            Property(c => c.FirstName).IsRequired();
            Property(c => c.LastName).IsRequired();
            Property(c => c.Email).IsRequired();

            Property(c => c.PostalCode).HasMaxLength(10);
            Property(c => c.PhoneNumber).HasMaxLength(12);

            Property(c => c.Notes).HasColumnType("nvarchar(1000)");
        }
    }
}