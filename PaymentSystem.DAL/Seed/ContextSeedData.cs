﻿using System.Data.Entity;
using PaymentSystem.DAL.Context;

namespace PaymentSystem.DAL.Seed
{
    class ContextSeedData : DropCreateDatabaseIfModelChanges<BillingContext>
    {
        protected override void Seed(BillingContext context)
        {
            base.Seed(context);
        }
    }
}