﻿using System.ComponentModel.DataAnnotations.Schema;
using PaymentsSystem.Core.Base;

namespace PaymentsSystem.Core
{
    public class Customer : BaseEntity
    {
        public int MerchantId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string Email { get; set; }

        public string Company { get; set; }

        public string DisplayName { get; set; }

        public string PhoneNumber { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string Address { get; set; }

        public string PostalCode { get; set; }

        public string Notes { get; set; }

        [ForeignKey("MerchantId")]
        public virtual Merchant Merchant { get; set; }
    }
}