﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using PaymentsSystem.Core.Base;

namespace PaymentsSystem.Core
{
    public class Payment : BaseEntity
    {
        public decimal Amount { get; set; }

        public int StatusId { get; set; }

        public DateTime PaymentDate { get; set; }

        public string Note { get; set; }

        public int InvoiceId { get; set; }

        public int CustomerId { get; set; }

        public int MerchantId { get; set; }

        [ForeignKey("InvoiceId")]
        public virtual Invoice Invoice { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("MerchantId")]
        public virtual Merchant Merchant { get; set; }

        public Payment()
        {
            PaymentDate = DateTime.Now;
        }
    }
}