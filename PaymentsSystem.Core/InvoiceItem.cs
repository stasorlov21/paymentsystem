﻿using System.ComponentModel.DataAnnotations.Schema;
using PaymentsSystem.Core.Base;

namespace PaymentsSystem.Core
{
    public class InvoiceItem : BaseEntity
    {
        public int InvoiceId { get; set; }

        public int OrderNumber { get; set; }

        public string Description { get; set; }

        public short Quantity { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal ItemTotal { get; set; }

        [ForeignKey("InvoiceId")]
        public virtual Invoice Invoice { get; set; }
    }
}