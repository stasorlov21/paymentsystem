﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using PaymentsSystem.Core.Base;

namespace PaymentsSystem.Core
{
    public class Invoice : BaseEntity
    {
        public int MerchantId { get; set; }

        public int CustomerId { get; set; }

        public int InvoiceNumber { get; set; }

        public DateTime InvoiceDate { get; set; }

        public DateTime DueDate { get; set; }

        public decimal TaxRate { get; set; }

        public decimal Discount { get; set; }

        public string Notes { get; set; }

        public decimal IntoiceTotal { get; set; }

        public int StatusId { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("MerchantId")]
        public virtual Merchant Merchant { get; set; }

        public virtual IList<InvoiceItem> Items { get; set; }

        public Invoice()
        {
            InvoiceDate = DateTime.Now;
            DueDate = DateTime.Now;
        }
    }
}