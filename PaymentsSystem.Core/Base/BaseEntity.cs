﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaymentsSystem.Core.Base
{
    public abstract class BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTime InsertedDate { get; set; }

        public DateTime LastUpdatedDate { get; set; }
    }
}