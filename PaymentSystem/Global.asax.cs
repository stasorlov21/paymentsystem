﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using PaymentsSystem.Core;
using PaymentSystem.App_Start;
using PaymentSystem.DAL;

namespace PaymentSystem
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            var context = new PaymentsSystemDbContext();

            var customers = new List<Customer>
                {
                    new Customer
                    {
                        Id = 1,
                        FirstName = "Stas",
                        LastName = "Orlov",
                        Company = "GL",
                        Email = "stasorlov21@gmail.com",
                        Phone = "1234567890",
                        Address = "address1"
                    },
                    new Customer
                    {
                        Id = 2,
                        FirstName = "Brant",
                        LastName = "Palazza",
                        Company = "ReliaBills",
                        Email = "bpalazza@reliabills.com",
                        Phone = "9876543210",
                        Address = "address2"
                    },
                    new Customer
                    {
                        Id = 3,
                        FirstName = "Bruce",
                        LastName = "Arp",
                        Company = "ReliaBills",
                        Email = "barp@reliabills.com",
                        Phone = "5432109876",
                        Address = "address3"
                    }
                };

            //foreach (var customer in customers)
            //{
            //    context.Customers.Add(customer);
            //}

            //context.SaveChanges();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}