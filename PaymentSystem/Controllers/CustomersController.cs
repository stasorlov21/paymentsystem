﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using PaymentsSystem.Core;
using PaymentSystem.DAL;

namespace PaymentSystem.Controllers
{
    public class CustomersController : ApiController
    {
        public IHttpActionResult Get()
        {
            var dbContext = new PaymentsSystemDbContext();

            return Json(dbContext.Customers.ToList());
        }

        // GET api/<controller>
        public IHttpActionResult Get(int pageSize, int pageNumber)
        {
            var dbContext = new PaymentsSystemDbContext();
            var query = dbContext.Set<Customer>();
            var total = 0;
            IEnumerable<Customer> customers = new List<Customer>();

            var page =
                dbContext.Customers.OrderBy(c => c.Id)
                    .Skip(pageNumber*pageSize)
                    .Take(pageSize)
                    .GroupBy(e => new {Total = query.Count()})
                    .FirstOrDefault();

            if (page != null)
            {
                total = page.Key.Total;
            }
            if (page != null)
            {
                customers = page.Select(c => c).AsEnumerable();
            }

            var result = new
            {
                TotalCount = total,
                TotalPages = (int) (Math.Ceiling((double) total/pageSize)),
                Customers = customers
            };

            return Json(result);
        }

        // GET api/<controller>/5
        public JsonResult<Customer> Get(int id)
        {
            var dbContext = new PaymentsSystemDbContext();

            var customer =
                dbContext.Customers.FirstOrDefault(cust => cust.Id == id);

            return Json(customer);
        }

        // POST api/<controller>
        public void Post([FromBody]Customer customer)
        {
            if (customer != null)
            {
                var dbContext = new PaymentsSystemDbContext();
                dbContext.Customers.Add(customer);
                dbContext.SaveChanges();
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]object value)
        {
            var customer = JsonConvert.DeserializeObject<Customer>(value.ToString());

            if (customer != null)
            {
                var dbContext = new PaymentsSystemDbContext();
                dbContext.Customers.Attach(customer);
                dbContext.Entry(customer).State = EntityState.Modified;
                dbContext.SaveChanges();
            }
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            var customer = new Customer {Id = id};
            var dbContext = new PaymentsSystemDbContext();
            dbContext.Customers.Attach(customer);
            dbContext.Customers.Remove(customer);
            dbContext.SaveChanges();
        }
    }
}