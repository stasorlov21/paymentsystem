﻿var paymentsApp = angular
    .module('paymentApp', [
        'ngRoute',
        'paymentApp.controllers.customers',
        'paymentApp.controllers.customerEdit',
        'paymentApp.controllers.customerAdd',
        'paymentApp.controllers.home',
        'paymentApp.controllers.invoices',
        'paymentApp.controllers.invoiceAdd',
        'paymentApp.controllers.invoiceEdit',
        'paymentApp.controllers.payments',
        'paymentApp.services.customers'
    ])
    .config(function ($routeProvider) {
    $routeProvider

        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'homeController'
        })

        .when('/customers', {
            templateUrl: 'views/customers/list.html',
            controller: 'customersController'
        })

        .when('/viewCustomer/:id', {
            templateUrl: 'views/customers/edit.html',
            controller: 'customerEditController'
        })

        .when('/addCustomer', {
            templateUrl: 'views/customers/edit.html',
            controller: 'customerAddController'
        })

        .when('/invoices', {
            templateUrl: 'views/invoices/list.html',
            controller: 'invoicesController'
        })

        .when('/addInvoice', {
            templateUrl: 'views/invoices/edit.html',
            controller: 'invoiceAddController'
        })

        .when('/viewInvoice/:id', {
            templateUrl: 'views/invoices/edit.html',
            controller: 'invoiceEditController'
        })

        .when('/payments', {
            templateUrl: 'views/payments/list.html',
            controller: 'paymentsController'
        });
});