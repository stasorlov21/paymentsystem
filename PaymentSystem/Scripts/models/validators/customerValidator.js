﻿function validateCustomer(customer) {
    if (!!customer) {
        
        return (customer.hasOwnProperty('Id') && typeof customer.Id === 'number' &&
            customer.hasOwnProperty('FirstName') && typeof customer.FirstName === 'string' &&
            customer.hasOwnProperty('LastName') && typeof customer.LastName === 'string' &&
            customer.hasOwnProperty('Company') && typeof customer.Company === 'string' &&
            customer.hasOwnProperty('Email') && typeof customer.Email === 'string' &&
            customer.hasOwnProperty('Phone') && typeof customer.Phone === 'string' &&
            customer.hasOwnProperty('Address') && typeof customer.Address === 'string');
    }

    return false;
}