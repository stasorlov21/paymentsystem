﻿function Customer(id, firstName, lastName, company, email, phone, address) {
    this.Id = id;
    this.FirstName = firstName;
    this.LastName = lastName;
    this.Company = company;
    this.Email = email;
    this.Phone = phone;
    this.Address = address;

    return this;
}

Customer.prototype.getFullName = function() {
    return this.FirstName + ' ' + this.LastName;
};