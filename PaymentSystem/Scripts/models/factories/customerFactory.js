﻿function customerFactory(responseObject) {
    var r = responseObject;

    if (validateCustomer(r)) {
        return new Customer(r.Id, r.FirstName, r.LastName, r.Company, r.Email, r.Phone, r.Address);
    }

    throw "Cannot obtain customer object from the server";
}