﻿function Invoice(customerId, invoiceNumber, invoiceDate, comments, taxRate, discount, details) {
    this.customerId = customerId;
    this.invoiceNumber = invoiceNumber;
    this.invoiceDate = invoiceDate;
    this.comments = comments;
    this.taxRate = taxRate;
    this.discount = discount;

    if (details instanceof Array) {
        this.details = details;
    } else {
        throw "Details should be an array";
    }

    return this;
}

Invoice.prototype.addDetail = function(detail) {
    this.details.push(detail);
}