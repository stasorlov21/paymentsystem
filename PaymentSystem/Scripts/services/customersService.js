﻿angular.module('paymentApp.services.customers', [])
    .factory('customersService', ['$http', function ($http) {

        var urlBase = '/api/customers/';
        var customerServiceFactory = {};

        customerServiceFactory.getCustomers = function() {
            return $http.get(urlBase).then(function(response) {
                return response.data;
            });
        };

        customerServiceFactory.findCustomers = function (pageSize, pageNumber) {
            if (!pageSize) {
                pageSize = 5;
            }

            if (!pageNumber) {
                pageNumber = 0;
            }

            return $http.get(urlBase + '?pageSize=' + pageSize + '&pageNumber=' + pageNumber).then(function (response) {
                var customers = [];

                response.data.Customers.forEach(function (element) {
                    customers.push(customerFactory(element));
                });

                return { totalPages: response.data.TotalPages, customers: customers };
            });
        };

        customerServiceFactory.getCustomer = function (id) {
            if (!!id && typeof id === 'number') {

                return $http.get(urlBase + '/' + id).then(function (response) {
                    var customer = customerFactory(response.data);

                    return customer;
                });

            } else {
                throw 'Customer Id should be a number';
            }
        };

        customerServiceFactory.updateCustomer = function (id, customer) {
            if (validateCustomer(customer)) {
                return $http.put(urlBase + id, customer)
                    .success(function (data, status, headers) {
                        console.log(data);
                    })
                    .error(function (data, status, headers) {
                        console.log(data);
                    });
            } else {
                console.log('Customer entity isn\'t valid');
                return false;
            }
        };

        customerServiceFactory.addCustomer = function (customer) {
            customer.Id = 0;
            if (validateCustomer(customer)) {
                return $http.post(urlBase, customer)
                    .success(function (data, status, headers) {
                        console.log(data);
                    })
                    .error(function (data, status, headers) {
                        console.log(data);
                    });
            } else {
                console.log('Customer entity isn\'t valid');
                return false;
            }
        };

        customerServiceFactory.deleteCustomer = function (id) {
            if (!!id && typeof id === 'number') {
                return $http.delete(urlBase + id)
                    .success(function (data, status, headers) {
                        console.log(data);
                    })
                    .error(function (data, status, headers) {
                        console.log(data);
                    });
            } else {
                throw 'Customer Id should be a number';
            }
        }

        return customerServiceFactory;
    }]);