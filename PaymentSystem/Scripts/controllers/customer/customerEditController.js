﻿angular.module('paymentApp.controllers.customerEdit', [])
    .controller('customerEditController', ['$scope', '$routeParams', '$location', 'customersService', function ($scope, $routeParams, $location, customerService) {

        $scope.customer = null;

        function _getCustomer() {
            var id = Number($routeParams.id);

            customerService.getCustomer(id)
                .then(function (customer) {
                    $scope.customer = customer;
                }, function (error) {
                    console.log(error);
                });
        }

        _getCustomer();

        $scope.update = function (customer) {
            var id = Number($routeParams.id);

            customerService.updateCustomer(id, customer)
                .then(function(sucess) {
                    console.log(sucess);
                    $scope.isSuccessfull = true;
                    $location.path('/customers');
                }, function(error) {
                    console.log(error);

                    $scope.isSuccessfull = false;
                });
        }

        $scope.reset = function() {
            $location.path('/customers');
        };
    }
]);