﻿angular.module('paymentApp.controllers.customers', [])
    .controller('customersController', ['$scope', '$location', 'customersService', function ($scope, $location, customerService) {

        $scope.customers = [];

        var pagedDictionary = {};

        function _getCustomers(pageSize, pageNumber) {
            customerService.findCustomers(pageSize, pageNumber)
                .then(function (customersObject) {
                    $scope.customers = customersObject.customers;

                    if (typeof pageNumber === 'number') {
                        pagedDictionary[pageNumber] = customersObject.customers;
                    }

                    var pages = [];
                    for (var i = 0; i < customersObject.totalPages; i++) {
                        pages[i] = i;
                    }

                    $scope.pages = pages;

            }, function (error) {
                    console.log(error);
                });
        }

        function _deleteCustomer(id) {
            if (confirm('Are you sure want to delete this Customer')) {
                customerService.deleteCustomer(id)
                    .then(function (success) {
                        var index = $scope.customers.indexOf(id);
                        $scope.customers.splice(index, 1);
                }, function (error) {
                        console.log(error);
                    });
            }
        }

        function _showPage(number) {

            if (number in pagedDictionary) {
                $scope.customers = pagedDictionary[number];
            } else {
                _getCustomers(5, number);
            }
        }

        _getCustomers();

        $scope.deleteCustomer = _deleteCustomer;
        $scope.showPage = _showPage;
    }
]);