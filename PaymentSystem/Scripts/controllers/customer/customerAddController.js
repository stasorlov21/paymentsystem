﻿angular.module('paymentApp.controllers.customerAdd', [])
    .controller('customerAddController', ['$scope', '$routeParams', '$location', 'customersService', function ($scope, $routeParams, $location, customerService) {

        $scope.update = function (customer) {

            customerService.addCustomer(customer)
                .then(function (sucess) {
                    console.log(sucess);
                    $scope.isSuccessfull = true;
                    $location.path('/customers');
                }, function (error) {
                    console.log(error);

                    $scope.isSuccessfull = false;
                });
        }

        $scope.reset = function () {
            $location.path('/customers');
        };
    }
]);