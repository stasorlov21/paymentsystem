﻿angular.module('paymentApp.controllers.invoiceAdd', [])
    .controller('invoiceAddController', ['$scope', 'customersService', function ($scope, customerService) {

        $scope.customers = [];

        function _getCustomers() {
            customerService.getCustomers()
                .then(function (customersObject) {
                    $scope.customers = customersObject;
                }, function (error) {
                    console.log(error);
                });
        }

    _getCustomers();

}]);