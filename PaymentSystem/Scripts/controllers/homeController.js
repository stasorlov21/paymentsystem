﻿angular.module('paymentApp.controllers.home', [])
    .controller('homeController', function ($scope) {
        $scope.welcome = 'Welcome in payment system!';

    $scope.links = [
        {
            title: '1) Angular Routing tutorial',
            href: 'https://scotch.io/tutorials/single-page-apps-with-angularjs-routing-and-templating'
        },
        {
            title: '2) AngularJs oficial tutorial',
            href: 'https://docs.angularjs.org/guide/'
        }
    ];
});